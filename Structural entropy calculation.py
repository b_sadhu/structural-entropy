#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import _pickle as cPickle
import matplotlib.pyplot as plt
with open('../pic_markov_6_sol_b3.txt','rb') as rfp:
    sort_clust = cPickle.load(rfp)

#calculates structural entropy, input: the markov file containing cluster information, provide number of nodes
# uses approach of Scientific Reports volume 9, Article number: 10832 (2019)

num_of_nodes = 120
counts_list = []
for each in sort_clust:
    cluster_count_list = []
    for i in range(len(each)):
        cluster_count_list.append(len(each[i]))
    counts, bins = np.histogram(cluster_count_list, bins=num_of_nodes, range=(1, num_of_nodes))
    counts_list.append(counts)
    
Strunctural_entropy_time_series = []
for each in range(len(counts_list)):
    list_entropy = []
    for index,num_of_clusters_of_same_size in enumerate(counts_list[each]):
    #print(each, index)
        size_of_clusters = (index + 1)
        if num_of_clusters_of_same_size > 0:
            p_i = (size_of_clusters)/num_of_nodes
            shannon_entropy = -p_i*log(p_i)
            sum_shannon_entropy = num_of_clusters_of_same_size*shannon_entropy
            
            list_entropy.append(sum_shannon_entropy)
    
    Strunctural_entropy = np.sum(list_entropy)           
    
    Strunctural_entropy_time_series.append(Strunctural_entropy)

print('average of structural_entropy:',np.sum(Strunctural_entropy_time_series)/len(Strunctural_entropy_time_series))
print(np.sum(Strunctural_entropy_time_series[901:])/100)

print('standard_deviation_structural_entropy:',np.std(Strunctural_entropy_time_series))
plt.plot(Strunctural_entropy_time_series)
plt.ylim(0,3.5)
plt.xlim(0,1000)

